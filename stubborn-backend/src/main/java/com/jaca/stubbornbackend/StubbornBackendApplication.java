package com.jaca.stubbornbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class StubbornBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(StubbornBackendApplication.class, args);
	}

}
