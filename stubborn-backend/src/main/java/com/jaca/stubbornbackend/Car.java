package com.jaca.stubbornbackend;

import lombok.Data;

@Data
public class Car {
    private final String name;
}
