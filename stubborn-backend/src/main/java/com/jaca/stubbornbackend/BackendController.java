package com.jaca.stubbornbackend;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;

@RestController
public class BackendController {

    @GetMapping("/car")
    public Mono<Car> car() {
        return Mono.just(new Car("Volvo"))
                .delayElement(Duration.ofMillis(100));
    }

}
