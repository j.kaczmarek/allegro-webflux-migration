Inspired by Allegro reactive migration
===
https://allegro.tech/2019/07/migrating-microservice-to-spring-webflux.html

* allegro-migration (a reactive app backed with a reactive backend service (webclient -> webflux controller))
* allegro-migration-block (a webmvc app backed with the same reactive backend service (restTemplate -> webflux controller))
* stubborn-backend - the backend service that serves a single simple domain object with a (non-blocking) delay

Some ideas
===

```
# blocking
ab -c 1000 -n 10000 http://localhost:8082/car    
# non-blocking
ab -c 1000 -n 10000 http://localhost:8080/car    
```

* observe no of threads (app logs)
* observe percentiles of response times from ab report