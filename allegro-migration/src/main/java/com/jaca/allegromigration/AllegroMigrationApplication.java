package com.jaca.allegromigration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Slf4j
@SpringBootApplication
@EnableScheduling
public class AllegroMigrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AllegroMigrationApplication.class, args);
	}

	@Scheduled(fixedDelay = 1000L)
	public void threads() {
		log.info(String.format("Threads: %s",Thread.activeCount()));
	}
}
