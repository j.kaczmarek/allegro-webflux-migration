package com.jaca.allegromigration;

import com.jaca.stubbornbackend.Car;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
public class FrontendCtrl {

    private final WebClient client;

    public FrontendCtrl(WebClient.Builder clientBuilder) {
        this.client = clientBuilder.baseUrl("http://localhost:8081").build();
    }

    @GetMapping("/car")
    public Mono<Car> car() {
        return this.client.get().uri("/car").retrieve().bodyToMono(Car.class);
    }

}
