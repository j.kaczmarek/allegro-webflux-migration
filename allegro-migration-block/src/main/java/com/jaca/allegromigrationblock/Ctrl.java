package com.jaca.allegromigrationblock;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequiredArgsConstructor
public class Ctrl {

    private final RestTemplate rest;

    @GetMapping("/car")
    public Car car() {
        return rest.getForObject("http://localhost:8081/car", Car.class);
    }

}

