package com.jaca.allegromigrationblock;

import lombok.Data;

@Data
public class Car {
    private String name;
}
