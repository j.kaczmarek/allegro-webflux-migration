package com.jaca.allegromigrationblock;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

@Slf4j
@SpringBootApplication
@EnableScheduling
public class AllegroMigrationBlockApplication {

	public static void main(String[] args) {
		SpringApplication.run(AllegroMigrationBlockApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Scheduled(fixedDelay = 1000L)
	public void threads() {
		log.info(String.format("Threads: %s",Thread.activeCount()));
	}
}
